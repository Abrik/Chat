from twisted.internet import reactor
from twisted.internet.protocol import ServerFactory, connectionDone
from twisted.protocols.basic import LineOnlyReceiver


class ServerProtocol(LineOnlyReceiver):
    factory: 'Server'
    login: str = None

    def connectionMade(self):
        self.factory.clients.append(self)

    def connectionLost(self, reason=connectionDone):
        self.factory.clients.remove(self)

    def lineReceived(self, line: bytes):
        content = line.strip().decode()

        print(content)
        if self.login is not None:
            content = f"Message {self.login}:{content}"
            if len(self.factory.messages)==10:
                self.factory.messages.pop(0)
            self.factory.messages.append(content)

            for user in self.factory.clients:
                if self != user:
                    user.sendLine(content.encode())
        else:
            # login:admin ->admin
            if content.startswith("login:"):
                userlogin = content.replace("login:", "")
                isAuth = True
                for user in self.factory.clients:
                    if user.login == userlogin:
                        isAuth = False

                if not isAuth:
                    self.sendLine(f"Login {userlogin} is exist".encode("UTF-8"))
                else:
                    self.login = userlogin
                    self.sendLine(f"Welcome {userlogin}".encode())
                    self.send_history()
            else:
                self.sendLine("Ivalid command".encode())

    def send_history(self):
        for msg in self.factory.messages:
            self.sendLine(msg.encode())


class Server(ServerFactory):
    protocol = ServerProtocol
    clients: list
    messages: list

    def startFactory(self):
        # super().startFactory()
        self.clients = []
        self.messages = []
        print("Server started")

    def stopFactory(self):
        print("Server stoped")


reactor.listenTCP(1234, Server())
reactor.run()
