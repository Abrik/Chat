#  Created by Artem Manchenkov
#  artyom@manchenkoff.me
#
#  Copyright © 2019
#
#  Графический PyQt 5 клиент для работы с сервером чата


from twisted.internet.protocol import ClientFactory
from twisted.protocols.basic import LineOnlyReceiver
import sys
from PyQt5 import QtWidgets, QtGui
from gui import design


class ConnectorProtocol(LineOnlyReceiver):
    factory: 'Connector'

    def connectionMade(self):
        #login:admin
        #self.sendLine("login:admin".encode())
        self.factory.window.protocol = self
        self.factory.window.plainTextEdit.appendPlainText("Connected")

    def lineReceived(self, line):
        self.factory.window.plainTextEdit.appendPlainText(line.decode())


class Connector(ClientFactory):
    protocol = ConnectorProtocol
    window: 'ChatWindow'

    def __init__(self,window) -> None:
        super().__init__()
        self.window = window


class ChatWindow(QtWidgets.QMainWindow, design.Ui_MainWindow):
    reactor = None
    protocol: ConnectorProtocol
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.init_handles()

    def init_handles(self):
        self.pushButton.clicked.connect(self.send_message)

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        self.reactor.callFromThread(self.reactor.stop)

    def send_message(self):
        message = self.lineEdit.text()
        #self.plainTextEdit.appendPlainText(message)
        self.protocol.sendLine(message.encode())
        self.lineEdit.clear()

app = QtWidgets.QApplication(sys.argv)

import qt5reactor

window = ChatWindow()
window.show()

qt5reactor.install()
from twisted.internet import reactor
reactor.connectTCP("127.0.0.1",1234,Connector(window))

window.reactor = reactor
reactor.run()
